
-- Hump Gamestate Library --
Gamestate = require("gamestate")

-- Your Gamestates --
Splash = require("src.splash")
Menu = require("src.menu")
Game = require("src.game")


function love.load()
  Gamestate.registerEvents()
  Gamestate.switch(Splash)
end
