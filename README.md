# Project Titan
A game demo made using the https://love2d.org framework.

Made specifically to show off how the dialogue boxes should look.
This was the first draft originally having the game set in space, with the rest of the game taking place in the player's spaceship, with Resident Evil-esque mechanics of scavenging parts from the environment to sustain the player's lifespan before reaching an eventual end, regardless of whether they reach the end of the game or not.

## Future plans

Rewrite into Unity using PS2 graphics, rather than sticking to the pixel game format, which LÖVE2d primarily supports and encourages.
