--! file :: main.moon
-- first thing that uhhhh love is gonna run lets gooooo

Moan = require "libraries.moan"
Camera = require "libraries.camera"
Sti = require "libraries.sti"
Gamestate = require "libraries.gamestate"

-- Your Gamestates --
Splash = require "src.splash"
Menu = require "src.menu"
Game = require "src.game"

class Game
	state =
		:playing,
		:dead,
		:menu

love.keyreleased = (key) ->
  -- Pass keypresses to Moan
  Moan.keyreleased(key)

  switch key
    when "f"
      Moan.advanceMsg!
    when "`"
      Moan.debug = not Moan.debug
    when "c"
      Moan.clearMessages!

  if key == "f"
    Moan.advanceMsg!
  elseif key == "`"
    Moan.debug = not Moan.debug
  elseif key == "c"
    Moan.clearMessages!

with love
	.load = ->
    Gamestate.registerEvents!
    Gamestate.switch Splash

		Moan.font = love.graphics.newFont("assets/Pixel UniCode.ttf", 32)

		-- Add font fallbacks for Japanese characters
		JPfallback = love.graphics.newFont("assets/JPfallback.ttf", 32)
		Moan.font\setFallbacks(JPfallback)


		-- Audio from bfxr (https://www.bfxr.net/)
		Moan.typeSound = love.audio.newSource("assets/typeSound.wav", "static")
		Moan.optionOnSelectSound = love.audio.newSource("assets/optionSelect.wav", "static")
		Moan.optionSwitchSound = love.audio.newSource("assets/optionSwitch.wav", "static")

		love.graphics.setBackgroundColor(0.39, 0.39, 0.39)
		math.randomseed(os.time())

		-- Make some objects
		p1 =
			x: 100,
			y: 200

		p2 =
		 x: 400,
		 y: 150

		p3 =
			x: 200,
			y: 300

		-- Create a HUMP camera and pass it to Moan
		camera = Camera(p1.x, p1.y)
		Moan.setCamera(camera)

		-- Set up our image for image argument in Moan.new config table
		--kasey = love.graphics.newImage "./assets/kasey.jpg"
		hana = love.graphics.newImage "./assets/hana.jpg"

		-- Put some messages into the Moan queue
		do
			Moan.speak(
				{"Kasey", {255,105,180}},
				{"u-uhm.. hiii <3", "*shits the bed violently*"},
			)
			Moan.speak(
				{"Hana", {255,105,180}},
				{"kys"},
				{
					onstart: => Moan.setSpeed("slow")
				}, {image: hana}
			)

		-- set a background image too
		main_bg = .graphics.newImage("./assets/base.jpg")

	.update = (dt) ->
		-- protagonist\update(dt)
		Moan.update(dt)

	.draw = ->
		-- protagonist\draw!
		Moan.draw!
